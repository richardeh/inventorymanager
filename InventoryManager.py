"""
InventoryManager.py
Manages an inventory of products. This would be more efficient with
a database, but the project is to build a product class and an inventory
class, and manage the inventory that way. 

Author: Richard Harrington
Created: 11/8/2013
"""

class Product(): 
    price=0
    id_num=0
    qty=0
    name=""
    def __init__(self,id_num,name,price,qty):
        self.price=price
        self.id_num=id_num
        self.name=name
        self.qty=qty

    def get_price(self):
        return self.price

    def get_qty(self):
        return self.qty

    def get_name(self):
        return self.name

    def set_qty(self,new_qty):
        # If quantity is zero or positive, update the quantity
        if new_qty>=0:
            self.qty=new_qty
            return "Quantity Updated"
        else:
            return "Insufficient Quantity"

    def set_price(self,new_price):
        # Update the price of a product
        self.price=new_price
        return "Price Updated"
    

class Inventory():
    products=[]

    def __init__(self):
        pass

    def add_product(self,product_name,price,quantity):
        # Creates a new product given a name, price and quantity to add
        self.products.append(Product(len(self.products),product_name,price,quantity))

    def update_quantity(self,product_name,new_qty):
        # Updates the quantity of a given product
        for product in self.products:
            if product.get_name()==product_name:
                print(product.set_qty(new_qty))
    
    def update_price(self,product_name,new_price):
        # Updates the price of a given product
        for p in self.products:
            if p.get_name()==product_name:
                print(p.set_price(new_price))

    def list_products(self):
        # Lists name, quantity, and price of all products
        for product in self.products:
            print(product.get_name(),product.get_qty(),"$",product.get_price())

    def get_product(self,product_name):
        # Prints name, quantity and price of a specified product
        for product in self.products:
            if product.get_name()==product_name:
                print(product.get_name(),product.get_qty(),"$",product.get_price())

    def get_inventory(self):
        value=0
        for product in self.products:
            value+=product.get_qty()*product.get_price()
        return value
    
        
    
myI=Inventory
myI.add_product(myI,"Tacos",1.99,2000)


                
